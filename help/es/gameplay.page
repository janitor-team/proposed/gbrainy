<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="gameplay" xml:lang="es">
      
  <info>
    <link type="guide" xref="index"/>
    <desc>Introducción a <app>gbrainy</app></desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.1" date="2010-01-21" status="review"/>
    <revision pkgversion="2.30" version="0.1" date="2010-04-05" status="review"/>
   <credit type="author">
    <name>Milo Casagrande</name>
    <email>milo@ubuntu.com</email>
   </credit>
   <license>
     <p>Creative Commons Compartir Igual 3.0</p>
   </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  </info>

  <title>Juego</title>
  
  <p><app>gbrainy</app> es un juego de preguntas capciosas para divertirse y mantener entrenado tu cerebro.</p>
  <p>Incluye diferentes tipos de juegos como puzles lógicos, juegos de cálculo mental, entrenadores de memoria y analogías verbales, diseñados para probar diferentes capacidades cognitivas.</p>
  <p>Puedes elegir entre diferentes niveles de dificultad haciendo que <app>gbrainy</app> sea divertido para niños, adultos o personas mayores. También tiene un histórico de las partidas del jugador, los récord personales del jugador, pistas durante la partida y soporte para jugar a pantalla completa. <app>gbrainy</app> también se puede <link href="https://wiki.gnome.org/Apps/gbrainy/Extending">extender</link> de forma fácil con juegos desarrollados por terceras partes.</p>
  <p><app>gbrainy</app> depende en gran medida del trabajo de los maestros de puzles desde los clásicos de tiempos ancestrales a las ideas de trabajos más recientes como los puzles de <link href="http://en.wikipedia.org/wiki/Terry_Stickels">Terry Stickels</link> o el clásico juego <link href="http://en.wikipedia.org/wiki/Dr._Brain">Dr. Brain</link>.</p>
  <note>
    <p>Ha habido recientes discusiones en la comunidad científica acerca de si jugar a juegos de preguntas capciosas mejora el rendimiento cognitivo. La mayoría de los estudios muestra que no hay, o a penas hay, una mejora, pero puedes pasar un buen rato jugando a juegos como <app>gbrainy</app>.</p>
  </note>
</page>
