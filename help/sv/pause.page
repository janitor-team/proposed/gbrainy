<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pause" xml:lang="sv">
      
  <info>
    <title type="sort">4</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="newgame"/>
    <desc>Hur man pausar eller avslutar ett spel.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons DelaLika 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Pausa/avsluta ett spel</title>

  <section id="pause-pause">
    <title>Pausa och fortsätta ett spel</title>
    <p>För att pausa ett spela så att du kan fortsätta vid samma punkt senare, gör något av följande:</p>
    <list>
      <item>
        <p>Välj <guiseq><gui style="menu">Spel</gui><gui style="menuitem">Pausa spel</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicka på knappen <gui style="button">Pausa</gui> i verktygsfältet.</p>
      </item>
    </list>
    <p>För att fortsätta spelet efter att du pausat det, gör något av följande:</p>
    <list>
      <item>
        <p>Välj <guiseq><gui style="menu">Spel</gui><gui style="menuitem">Pausa spel</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicka på knappen <gui style="button">Fortsätt</gui> i verktygsfältet.</p>
      </item>
    </list>
  </section>
  
  <section id="pause-stop">
    <title>Avsluta ett spel</title>
    <p>För att avsluta ett spel, gör något av följande:</p>
    <list>
      <item>
        <p>Välj <guiseq><gui style="menu">Spel</gui><gui style="menuitem">Avsluta spel</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicka på knappen <gui style="button">Avsluta</gui> i verktygsfältet.</p>
      </item>
    </list>
  </section>
</page>
