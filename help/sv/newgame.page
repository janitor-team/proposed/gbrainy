<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="newgame" xml:lang="sv">
      
  <info>
    <title type="sort">1</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gametypes"/>
    <desc>Starta och spela ett spel.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>
   <credit type="author">
    <name>Milo Casagrande</name>
    <email>milo@ubuntu.com</email>
   </credit>
   <license>
     <p>Creative Commons DelaLika 3.0</p>
   </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Spelsession</title>
  
  <section id="game-start">
    <title>Starta en ny session</title>
    <p>För att starta ett nytt spel, gör ett av följande:</p>
    <list>
      <item>
        <p>Välj <guiseq><gui style="menu">Spel</gui><gui style="menuitem">Nytt spel</gui></guiseq> och sedan speltyp att spela.</p>
      </item>
      <item>
        <p>Klicka på en av knapparna i verktygsfältet.</p>
      </item>
    </list>
    <p>Knapparna i verktygsfältet har följande betydelser:</p>
    <table frame="none" rules="none" shade="none">
      <tr>
        <td>
        <terms>
          <item>
            <title>Alla</title>
            <p>Startar ett nytt spel med alla tillgängliga speltyper.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/all-games.png" its:translate="no">
            <p>
              All games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Logik</title>
            <p>Startar ett nytt spel med endast logikspel.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
            <p>
            Logic games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Beräkning</title>
            <p>Startar ett nytt spel med bara beräkningsspel.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
            <p>
              Calculation games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Minne</title>
            <p>Startar ett nytt spel med bara minnesspel.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
            <p>
              Memory games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Verbal</title>
            <p>Startar ett nytt spel med endast verbala analogispel.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
            <p>
              Verbal games button
            </p>
          </media>
        </td>
      </tr>
    </table>
    <note>
      <p>Beskrivningarna gäller också för speltyperna du kan välja i <gui style="menu">Spel</gui>-menyn.</p>
    </note>
  </section>
  <section id="game-play">
    <title>Spela en omgång</title>
    <note style="tip">
      <p>När du spelar, läs alltid instruktionerna noggrant!</p>
    </note>
    <p>Spelomgången börjar genom att visa dig problemet och fråga efter svaret. Längst ned i fönstret finns huvudkontrollerna du kan använda för att interagera med spelet.</p>
    <p>Så fort du vet svaret på problemet, skriv in det i <gui style="input">Svar</gui>-fältet och tryck <gui style="button">OK</gui>.</p>
    <p>För att fortsätta till nästa spel, klicka <gui style="button">Nästa</gui>.</p>
    <note>
      <p>Om du klickar <gui style="button">Nästa</gui> innan du avslutat spelet eller utan att gett ett svar kommer det spelet att räknas med till dina resultat.</p>
    </note>
  </section>
</page>
