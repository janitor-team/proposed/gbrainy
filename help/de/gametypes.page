<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="gametypes" xml:lang="de">
  <info>
    <title type="sort">3</title>
    <desc>Verschiedene Spiele, die Sie spielen können.</desc>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gameplay"/>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>    
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Paul Seyfert</mal:name>
      <mal:email>pseyfert@mathphys.fsk.uni-heidelberg.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>
  <title>Spielarten</title>

  <p><app>gbrainy</app> bietet die folgenden Spielarten:</p>
  
  <table frame="none" rules="none" shade="none">
    <tr>
      <td>
      <terms>
        <item>
          <title>Logikrätsel</title>
          <p>Aufgaben, die Ihr Schlussfolgerungs- und Denkvermögen herausfordern. Diese Rätsel basieren auf Sequenzen von Elementen, visuellen und räumlichen Schlussfolgerungen oder Zusammenhängen zwischen Elementen.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
          <p>
            Logic games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Übungen zum Kopfrechnen</title>
          <p>Aufgaben, die auf arithmetischen Operationen beruhen, um Ihre Kopfrechenfähigkeiten zu verbessern. Der Spieler hat bei diesen Übungen Multiplikation, Division, Addition und Subtraktion zu kombinieren und auf unterschiedliche Weise zu verwenden.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
          <p>
            Calculation games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Gedächtnisspiele</title>
          <p>Aufgaben, die Ihr Kurzzeitgedächtnis auf die Probe stellen. Bei diesen Übungen handelt es sich um Sammlungen von Objekten, die sich der Spieler merken soll. Manchmal sind es Zusammenhänge zwischen Elementen, Wörter, Zahlen oder Farben.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
          <p>
            Memory games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Verbale Analogien</title>
          <p>Spiele, die Ihr verbales Geschick fordern. Diese Aufgaben fordern den Spieler auf, Ursachen und Auswirkungen herauszufinden, Synonyme oder Gegenteile zu verwenden und prüfen das Vokabular.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
          <p>
            Verbal analogies games logo
          </p>
        </media>
      </td>
    </tr>
  </table>
</page>
