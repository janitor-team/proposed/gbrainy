# German translation for gbrainy.
# Copyright (C) 2009 gbrainy's COPYRIGHT HOLDER
# This file is distributed under the same license as the gbrainy package.
# Wolfgang Stöggl <c72578@yahoo.de>, 2009-2011.
# Christian Kirbach <Christian.Kirbach@googlemail.com>, 2010.
# Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>, 2011.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2017-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: gbrainy help master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?\n"
"POT-Creation-Date: 2018-06-10 16:10+0000\n"
"PO-Revision-Date: 2018-08-25 12:45+0200\n"
"Last-Translator: Tim Sabsch <tim@sabsch.com>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.1.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Wolfgang Stöggl <c72578@yahoo.de>, 2010\n"
"Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>, 2011\n"
"Mario Blättermann <mario.blaettermann@gmail.com>, 2017, 2018"

#. (itstool) path: info/desc
#: C/customgame.page:9
msgid "Start a custom games selection."
msgstr "Eine eigene Auswahl an Spielen starten."

#. (itstool) path: credit/name
#: C/customgame.page:12 C/pause.page:12 C/tips.page:12 C/difficulty.page:12
#: C/history.page:9 C/multiplayer.page:11 C/score.page:11 C/gameplay.page:12
#: C/index.page:15 C/newgame.page:14 C/times.page:11 C/export.page:10
msgid "Milo Casagrande"
msgstr "Milo Casagrande"

#. (itstool) path: license/p
#: C/customgame.page:16 C/gametypes.page:13 C/pause.page:16 C/tips.page:16
#: C/difficulty.page:16 C/history.page:13 C/multiplayer.page:15 C/score.page:15
#: C/gameplay.page:16 C/index.page:19 C/newgame.page:18 C/times.page:15
#: C/export.page:14
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: page/title
#: C/customgame.page:20
msgid "Custom games selection"
msgstr "Eigene Spielauswahl"

#. (itstool) path: page/p
#: C/customgame.page:21
msgid ""
"It is possible to play a custom selection of all the games in <app>gbrainy</"
"app>."
msgstr ""
"Es ist möglich, eine eigene Auswahl an Übungen in <app>gbrainy</app> zu "
"erstellen."

#. (itstool) path: page/p
#: C/customgame.page:24
msgid "To do so, proceed as follows:"
msgstr "Um dies zu erreichen, gehen Sie wie folgt vor:"

#. (itstool) path: item/p
#: C/customgame.page:29
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New "
"Game</gui> <gui style=\"menuitem\">Custom Game Selection</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Spiel</gui><gui style=\"menuitem"
"\">Neues Spiel</gui><gui style=\"menuitem\">Eigenes Spiel erstellen</gui></"
"guiseq>."

#. (itstool) path: item/p
#: C/customgame.page:35
msgid ""
"In the <gui>Custom Game</gui> dialog, select the games that you would like "
"to play by clicking on the check box next to the name of the game."
msgstr ""
"Wählen Sie im Dialog <gui>Eigenes Spiel</gui> die Spiele, die Sie gerne "
"spielen möchten, durch Klicken auf das Ankreuzfeld neben dem Namen des "
"Spiels."

#. (itstool) path: item/p
#: C/customgame.page:41
msgid ""
"Once you are done, click on <gui style=\"button\">Start</gui> to start "
"playing your custom games selection."
msgstr ""
"Sobald Sie fertig sind, klicken Sie auf <gui style=\"button\">Start</gui>, "
"um mit dem Spielen Ihrer eigenen Auswahl zu beginnen."

#. (itstool) path: info/title
#: C/gametypes.page:6
msgctxt "sort"
msgid "3"
msgstr "3"

#. (itstool) path: info/desc
#: C/gametypes.page:7
msgid "What types of game you can play."
msgstr "Verschiedene Spiele, die Sie spielen können."

#. (itstool) path: page/title
#: C/gametypes.page:16
msgid "Game types"
msgstr "Spielarten"

#. (itstool) path: page/p
#: C/gametypes.page:18
msgid "<app>gbrainy</app> provides the following types of games:"
msgstr "<app>gbrainy</app> bietet die folgenden Spielarten:"

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:27 C/score.page:39
msgid "Logic puzzles"
msgstr "Logikrätsel"

#. (itstool) path: item/p
#: C/gametypes.page:28
msgid ""
"Games designed to challenge your reasoning and thinking skills. These games "
"are based on sequences of elements, visual and spatial reasoning or "
"relationships between elements."
msgstr ""
"Aufgaben, die Ihr Schlussfolgerungs- und Denkvermögen herausfordern. Diese "
"Rätsel basieren auf Sequenzen von Elementen, visuellen und räumlichen "
"Schlussfolgerungen oder Zusammenhängen zwischen Elementen."

#. (itstool) path: item/title
#: C/gametypes.page:48
msgid "Mental calculations"
msgstr "Übungen zum Kopfrechnen"

#. (itstool) path: item/p
#: C/gametypes.page:49
msgid ""
"Games based on arithmetical operations designed to improve your mental "
"calculation skills. Games that require the player to use multiplication, "
"division, addition and subtraction combined in different ways."
msgstr ""
"Aufgaben, die auf arithmetischen Operationen beruhen, um Ihre "
"Kopfrechenfähigkeiten zu verbessern. Der Spieler hat bei diesen Übungen "
"Multiplikation, Division, Addition und Subtraktion zu kombinieren und auf "
"unterschiedliche Weise zu verwenden."

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:69 C/score.page:51
msgid "Memory trainers"
msgstr "Gedächtnisspiele"

#. (itstool) path: item/p
#: C/gametypes.page:70
msgid ""
"Games designed to challenge your short-term memory. These games show "
"collections of objects and ask the player to recall them, sometimes "
"establishing relationships between figures, words, numbers or colors."
msgstr ""
"Aufgaben, die Ihr Kurzzeitgedächtnis auf die Probe stellen. Bei diesen "
"Übungen handelt es sich um Sammlungen von Objekten, die sich der Spieler "
"merken soll. Manchmal sind es Zusammenhänge zwischen Elementen, Wörter, "
"Zahlen oder Farben."

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:90 C/score.page:57
msgid "Verbal analogies"
msgstr "Verbale Analogien"

#. (itstool) path: item/p
#: C/gametypes.page:91
msgid ""
"Games that challenge your verbal aptitude. These games ask the player to "
"identify cause and effect, use synonyms or antonyms, and use their "
"vocabulary."
msgstr ""
"Spiele, die Ihr verbales Geschick fordern. Diese Aufgaben fordern den "
"Spieler auf, Ursachen und Auswirkungen herauszufinden, Synonyme oder "
"Gegenteile zu verwenden und prüfen das Vokabular."

#. (itstool) path: info/desc
#: C/license.page:8
msgid "Legal information."
msgstr "Rechtliche Hinweise."

#. (itstool) path: page/title
#: C/license.page:11
msgid "License"
msgstr "Lizenz"

#. (itstool) path: page/p
#: C/license.page:12
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 "
"Unported license."
msgstr ""
"Diese Arbeit ist lizenziert unter einer Creative Commons Attribution-Share "
"Alike 3.0 Unported License."

#. (itstool) path: page/p
#: C/license.page:18
msgid "You are free:"
msgstr "Es ist Ihnen gestattet:"

#. (itstool) path: item/title
#: C/license.page:23
msgid "To share"
msgstr "Freizugeben"

#. (itstool) path: item/p
#: C/license.page:24
msgid "To copy, distribute and transmit the work."
msgstr ""
"Das Werk bzw. den Inhalt zu vervielfältigen, zu verbreiten und öffentlich "
"zugänglich zu machen."

#. (itstool) path: item/title
#: C/license.page:27
msgid "To remix"
msgstr "Änderungen vorzunehmen"

#. (itstool) path: item/p
#: C/license.page:28
msgid "To adapt the work."
msgstr "Abwandlungen und Bearbeitungen des Werkes bzw. Inhaltes anzufertigen."

#. (itstool) path: page/p
#: C/license.page:31
msgid "Under the following conditions:"
msgstr "Unter den folgenden Bedingungen:"

#. (itstool) path: item/title
#: C/license.page:36
msgid "Attribution"
msgstr "Zuschreibung"

#. (itstool) path: item/p
#: C/license.page:37
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use "
"of the work)."
msgstr ""
"Sie dürfen das Werk nur unter gleichen Bedingungen weitergeben, wie Sie vom "
"Autor oder Lizenzgeber festgelegt wurden (aber nicht so, dass es wie Ihr "
"Werk aussieht)."

#. (itstool) path: item/title
#: C/license.page:44
msgid "Share Alike"
msgstr "Share Alike"

#. (itstool) path: item/p
#: C/license.page:45
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr ""
"Wenn Sie das lizenzierte Werk bzw. den lizenzierten Inhalt bearbeiten, "
"abwandeln oder in anderer Weise erkennbar als Grundlage für eigenes Schaffen "
"verwenden, dürfen Sie die daraufhin neu entstandenen Werke bzw. Inhalte nur "
"unter Verwendung von Lizenzbedingungen weitergeben, die mit denen dieses "
"Lizenzvertrages identisch, vergleichbar oder kompatibel sind."

#. (itstool) path: page/p
#: C/license.page:51
msgid ""
"For the full text of the license, see the <link href=\"https://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</"
"link>, or read the full <link href=\"https://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."
msgstr ""
"Den vollständigen Text der Lizenz finden Sie auf der <link href=\"https://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons-Webseite</"
"link>. Oder Sie können den vollständigen <link href=\"https://"
"creativecommons.org/licenses/by-sa/3.0/\">Commons Deed</link> lesen."

#. (itstool) path: info/title
#: C/pause.page:6
msgctxt "sort"
msgid "4"
msgstr "4"

#. (itstool) path: info/desc
#: C/pause.page:9
msgid "How to pause or end a game."
msgstr "Wie man ein Spiel anhält oder beendet."

#. (itstool) path: page/title
#: C/pause.page:20
msgid "Pause/End a game"
msgstr "Anhalten/Beenden eines Spiels"

#. (itstool) path: section/title
#: C/pause.page:23
msgid "Pause and resume a game"
msgstr "Anhalten und Fortsetzen eines Spiels"

#. (itstool) path: section/p
#: C/pause.page:24
msgid ""
"To pause a game so that you can resume it at the same point at a later time, "
"perform one of the following:"
msgstr ""
"Um ein Spiel anzuhalten, so dass Sie es zu einem späteren Zeitpunkt wieder "
"fortsetzen können, führen Sie einen der folgenden Schritte durch:"

#. (itstool) path: item/p
#: C/pause.page:30 C/pause.page:45
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">Pause "
"Game</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Spiel</gui><gui style=\"menuitem"
"\">Spiel pausieren / fortsetzen</gui></guiseq>."

#. (itstool) path: item/p
#: C/pause.page:35
msgid "Click on the <gui style=\"button\">Pause</gui> button in the toolbar."
msgstr ""
"Klicken Sie auf den <gui style=\"button\">Pause</gui>-Knopf in der "
"Werkzeugleiste."

#. (itstool) path: section/p
#: C/pause.page:40
msgid ""
"In order to resume the game after you paused it, perform one of the "
"following:"
msgstr ""
"Um ein Spiel fortzusetzen, nachdem es pausiert wurde, führen Sie einen der "
"folgenden Schritte durch:"

#. (itstool) path: item/p
#: C/pause.page:50
msgid "Click on the <gui style=\"button\">Resume</gui> button in the toolbar."
msgstr ""
"Klicken Sie auf den <gui style=\"button\">Fortsetzen</gui>-Knopf in der "
"Werkzeugleiste."

#. (itstool) path: section/title
#: C/pause.page:58
msgid "End a game"
msgstr "Ein Spiel beenden"

#. (itstool) path: section/p
#: C/pause.page:59
msgid "To end a game, perform one of the following:"
msgstr "Um ein Spiel zu beenden, führen Sie folgende Schritte durch:"

#. (itstool) path: item/p
#: C/pause.page:64
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">End "
"Game</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Spiel</gui><gui style=\"menuitem"
"\">Spiel beenden</gui></guiseq>."

#. (itstool) path: item/p
#: C/pause.page:69
msgid "Click on the <gui style=\"button\">End</gui> button in the toolbar."
msgstr ""
"Klicken Sie auf den <gui style=\"button\">Beenden</gui>-Knopf in der "
"Werkzeugleiste."

#. (itstool) path: info/desc
#: C/tips.page:8
msgid "Use the tips to solve a puzzle."
msgstr "Verwenden Sie die Hinweise, um ein Rätsel zu lösen."

#. (itstool) path: page/title
#: C/tips.page:20
msgid "Tips"
msgstr "Tipps"

#. (itstool) path: page/p
#: C/tips.page:21
msgid ""
"With some of the games you can play, it is possible to get simple tips that "
"can help you to solve the problem."
msgstr ""
"Bei manchen Spielen werden einfache Hinweise angeboten, die beim Lösen des "
"Problems hilfreich sein können."

#. (itstool) path: page/p
#: C/tips.page:25
msgid ""
"When playing a game, click on the <gui style=\"button\">Tip</gui> button at "
"the bottom of the window (next to the <gui style=\"input\">Answer</gui> text "
"entry)."
msgstr ""
"Klicken Sie während des Spiels auf den <gui style=\"button\">Hinweis</gui>-"
"Knopf im unteren Bereich des Fensters (neben der <gui style=\"input"
"\">Antwort</gui>-Texteingabe)."

#. (itstool) path: note/p
#: C/tips.page:29
msgid "This feature is not available for some of the games."
msgstr "Diese Funktion steht bei einigen Spielen nicht zur Verfügung."

#. (itstool) path: info/title
#: C/difficulty.page:6
msgctxt "sort"
msgid "2"
msgstr "2"

#. (itstool) path: info/desc
#: C/difficulty.page:8
msgid "Change the difficulty level of the games."
msgstr "Ändern des Schwierigkeitsgrades von Spielen."

#. (itstool) path: page/title
#: C/difficulty.page:20
msgid "Difficulty levels"
msgstr "Schwierigkeitsgrade"

#. (itstool) path: page/p
#: C/difficulty.page:21
msgid "To change the difficulty level of the game, proceed as follows:"
msgstr ""
"Um den Schwierigkeitsgrad eines Spiels zu ändern, gehen Sie wie folgt vor:"

#. (itstool) path: item/p
#: C/difficulty.page:26 C/times.page:26
msgid ""
"Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem"
"\">Preferences</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Einstellungen</gui><gui style="
"\"menuitem\">Einstellungen</gui></guiseq>."

#. (itstool) path: item/p
#: C/difficulty.page:31
msgid "In the <gui>Difficulty Level</gui> section, select the desired level."
msgstr ""
"Im Abschnitt <gui>Schwierigkeitsgrad</gui> können Sie die gewünschte Stufe "
"wählen."

#. (itstool) path: item/p
#: C/difficulty.page:34
msgid ""
"You can choose from three different levels: <gui style=\"radiobutton\">Easy</"
"gui>, <gui style=\"radiobutton\">Medium</gui>, and <gui style=\"radiobutton"
"\">Master</gui>. The default level is <gui style=\"radiobutton\">Medium</"
"gui>."
msgstr ""
"Sie können aus drei verschiedenen Stufen auswählen: <gui style=\"radiobutton"
"\">Leicht</gui>, <gui style=\"radiobutton\">Mittel</gui> und <gui style="
"\"radiobutton\">Schwer</gui>. Die Standardstufe ist <gui style=\"radiobutton"
"\">Mittel</gui>."

#. (itstool) path: info/desc
#: C/history.page:6
msgid "Access your personal game history."
msgstr "So gelangen Sie zu Ihrer persönlichen Spielchronik."

#. (itstool) path: page/title
#: C/history.page:17
msgid "Personal game history"
msgstr "Persönliche Spielchronik"

#. (itstool) path: page/p
#: C/history.page:19
msgid ""
"The <gui>Player's Game History</gui> dialog shows your performance in every "
"game type during the last few game sessions."
msgstr ""
"Der Dialog <gui>Spielchronik des Spielers</gui> zeigt Ihre Leistung in jedem "
"Spiel während der letzten Spielsitzungen."

#. (itstool) path: section/title
#: C/history.page:25
msgid "View the games history"
msgstr "Spielchronik ansehen"

#. (itstool) path: section/p
#: C/history.page:26
msgid ""
"To access your personal game history, choose <guiseq><gui style=\"menu"
"\">View</gui> <gui style=\"menuitem\">Player's Game History</gui></guiseq>."
msgstr ""
"Um zu Ihrer persönlichen Spielchronik zu gelangen, wählen Sie <guiseq><gui "
"style=\"menu\">Ansicht</gui><gui style=\"menuitem\">Spielchronik des "
"Spielers</gui></guiseq>."

#. (itstool) path: section/title
#: C/history.page:33
msgid "Select which results to show"
msgstr "Wählen Sie, welche Ergebnisse angezeigt werden sollen"

#. (itstool) path: section/p
#: C/history.page:34
msgid ""
"To select the results that will be shown in the graphic, use the different "
"check boxes located under the graph."
msgstr ""
"Verwenden Sie die verschiedenen Ankreuzfelder unterhalb der Grafik, um die "
"Ergebnisse auszuwählen, die in der Grafik angezeigt werden sollen."

#. (itstool) path: section/title
#: C/history.page:41
msgid "Change the number of saved games"
msgstr "Anzahl der gespeicherten Spiele ändern"

#. (itstool) path: section/p
#: C/history.page:42
msgid ""
"<app>gbrainy</app> saves the player's scores so it can track how they evolve."
msgstr ""
"<app>gbrainy</app> speichert die Ergebnisse des Spielers, so dass diese "
"mitverfolgt werden können."

#. (itstool) path: section/p
#: C/history.page:45
msgid ""
"In order to change the number of sessions recorded or how many games will be "
"stored in the history, proceed as follows:"
msgstr ""
"Um die Anzahl an gespeicherten Spielsitzungen zu ändern oder auch wie viele "
"Spiele in der Chronik gespeichert werden, gehen Sie wie folgt vor:"

#. (itstool) path: item/p
#: C/history.page:51
msgid ""
"Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem\"> "
"Preferences</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Einstellungen</gui><gui style="
"\"menuitem\">Einstellungen</gui></guiseq>."

#. (itstool) path: item/p
#: C/history.page:57
msgid "In the <gui>Player's Game History</gui> section:"
msgstr "Im Abschnitt <gui>Spielchronik des Spielers</gui>:"

#. (itstool) path: item/p
#: C/history.page:62
msgid ""
"Use the first spin box to change how many game sessions need to be recorded "
"before you can start to see the results in the history graphic."
msgstr ""
"Verwenden Sie das erste Einstellfeld, um die Anzahl an Spielsitzungen zu "
"ändern, die gespeichert werden müssen, so dass die Ergebnisse erstmals in "
"der Verlaufsgrafik angezeigt werden."

#. (itstool) path: item/p
#: C/history.page:68
msgid ""
"Use the second spin box to change how many game sessions will be shown in "
"the history graphic."
msgstr ""
"Das zweite Einstellfeld dient zur Änderung der Anzahl an Spielsitzungen, die "
"in der Verlaufsgrafik angezeigt werden."

#. (itstool) path: info/desc
#: C/multiplayer.page:7
msgid "How to play with other people."
msgstr "Spielen mit mehreren Teilnehmern."

#. (itstool) path: page/title
#: C/multiplayer.page:19
msgid "Play with other people"
msgstr "Spielen mit mehreren Teilnehmern"

#. (itstool) path: page/p
#: C/multiplayer.page:21
msgid ""
"It is not possible to play against other people over the Internet or a local "
"network with <app>gbrainy</app>."
msgstr ""
"Mit <app>gbrainy</app> ist es nicht möglich, gegen andere Personen über das "
"Internet oder ein lokales Netzwerk zu spielen."

#. (itstool) path: info/desc
#: C/score.page:8
msgid "How the player scores are calculated."
msgstr "Wie die Ergebnisse eines Spielers berechnet werden."

#. (itstool) path: page/title
#: C/score.page:19
msgid "Game score and timings"
msgstr "Spielergebnisse und Zeitmessungen"

#. (itstool) path: page/p
#: C/score.page:20
msgid "If you answer a puzzle incorrectly, you will not get any score for it."
msgstr ""
"Wenn Sie eine Aufgabe falsch beantworten, so erhalten Sie keine Punkte dafür."

#. (itstool) path: page/p
#: C/score.page:23
msgid ""
"If you answer a puzzle correctly, you will get a score which depends on the "
"time taken to resolve the problem and whether you used the tip during the "
"game."
msgstr ""
"Wenn Sie eine Aufgabe korrekt beantworten, erhalten Sie Punkte, die sowohl "
"von der Zeit abhängig sind, die für die Lösung des Problems benötigt wurde "
"als auch davon, ob ein Hinweis während des Spiels zu Rate gezogen wurde."

#. (itstool) path: page/p
#: C/score.page:27
msgid ""
"The following table summarizes the different game durations (in seconds) "
"based on the difficulty level."
msgstr ""
"Folgende Tabelle fasst die unterschiedlichen Spielzeiten (in Sekunden) in "
"Abhängigkeit vom Schwierigkeitsgrad zusammen."

#. (itstool) path: td/p
#: C/score.page:34
msgid "Easy"
msgstr "Leicht"

#. (itstool) path: td/p
#: C/score.page:35
msgid "Medium"
msgstr "Mittel"

#. (itstool) path: td/p
#: C/score.page:36
msgid "Master"
msgstr "Schwer"

#. (itstool) path: td/p
#: C/score.page:40
msgid "156"
msgstr "156"

#. (itstool) path: td/p
#: C/score.page:41
msgid "120"
msgstr "120"

#. (itstool) path: td/p
#: C/score.page:42
msgid "110"
msgstr "110"

#. (itstool) path: td/p
#: C/score.page:45
msgid "Mental calculation"
msgstr "Übungen zum Kopfrechnen"

#. (itstool) path: td/p
#: C/score.page:46
msgid "78"
msgstr "78"

#. (itstool) path: td/p
#: C/score.page:47
msgid "60"
msgstr "60"

#. (itstool) path: td/p
#: C/score.page:48
msgid "55"
msgstr "55"

#. (itstool) path: td/p
#: C/score.page:52 C/score.page:58
msgid "39"
msgstr "39"

#. (itstool) path: td/p
#: C/score.page:53 C/score.page:59
msgid "30"
msgstr "30"

#. (itstool) path: td/p
#: C/score.page:54 C/score.page:60
msgid "27"
msgstr "27"

#. (itstool) path: page/p
#: C/score.page:63
msgid ""
"With the expected time for the chosen difficulty level and the time you take "
"to complete the game, the following logic is applied:"
msgstr ""
"Basierend auf der für den gewählten Schwierigkeitsgrad erwarteten Zeit und "
"der für die Übung benötigten Zeit, wird folgende Logik verwendet:"

#. (itstool) path: item/p
#: C/score.page:69
msgid ""
"If you take less than the time expected to complete the game, you score 10 "
"points."
msgstr ""
"Wenn Sie schneller als die erwartete Zeit sind, erreichen Sie 10 Punkte."

#. (itstool) path: item/p
#: C/score.page:74
msgid ""
"If you take more than the time expected to complete the game, you score 8 "
"points."
msgstr ""
"Wenn Sie langsamer als die erwartete Zeit sind, erreichen Sie 8 Punkte."

#. (itstool) path: item/p
#: C/score.page:79
msgid ""
"If you take more than 2x the time expected to complete the game, you score 7 "
"points."
msgstr ""
"Wenn Sie mehr als doppelt so lange brauchen wie die erwartete Zeit, "
"erreichen Sie 7 Punkte."

#. (itstool) path: item/p
#: C/score.page:85
msgid ""
"If you take more than 3x the time expected to complete the game, you score 6 "
"points."
msgstr ""
"Wenn Sie mehr als dreimal so lange brauchen wie die erwartete Zeit, "
"erreichen Sie 6 Punkte."

#. (itstool) path: item/p
#: C/score.page:91
msgid "If you use a tip, you score only the 80% of the original score."
msgstr ""
"Wenn Sie einen Hinweis verwenden, erhalten Sie nur 80 % des ursprünglichen "
"Ergebnisses."

#. (itstool) path: section/title
#: C/score.page:98
msgid "Computing the totals"
msgstr "Berechnung der Gesamtergebnisse"

#. (itstool) path: section/p
#: C/score.page:99
msgid ""
"<app>gbrainy</app> keeps track of the different games types played. To "
"compute the final score of every set of game types it sums all the results "
"of the same game types played and then applies a factor based on: the "
"logarithm of 10 for the easy level; on the logarithm of 20 for the medium "
"level; and on the logarithm of 30 for the master level."
msgstr ""
"<app>gbrainy</app> protokolliert die verschiedenen gespielten Spielarten "
"mit. Zur Berechnung des Endergebnisses eines jeden Satzes an Spielarten "
"werden alle Ergebnisse derselben Spielart addiert und im Anschluss ein "
"Faktor angewandt, der auf dem Logarithmus von 10 für den leichten "
"Schwierigkeitsgrad, dem Logarithmus von 20 für den mittleren und dem "
"Logarithmus von 30 für den schweren beruht."

#. (itstool) path: section/p
#: C/score.page:106
msgid ""
"This means that when playing at medium difficulty level, to get a score of "
"100 points you need to score 10 points on at least 20 games of every game "
"type played."
msgstr ""
"Das bedeutet, dass man bei mittlerem Schwierigkeitsgrad in mindestens 20 "
"Spielen jeder Spielart 10 Punkte erreichen muss, um ein Endergebnis von 100 "
"Punkten zu erhalten."

#. (itstool) path: section/p
#: C/score.page:110
msgid ""
"This may sound challenging, but it allows players to compare game scores "
"from different sessions (in the player's game history) and allows better "
"tracking of the progression of the player through all of the games played."
msgstr ""
"Dies mag sich anspruchsvoll anhören, erlaubt Spielern allerdings, Ergebnisse "
"aus verschiedenen Sitzungen in der Spielchronik zu vergleichen und den "
"Fortschritt des Spielers in allen absolvierten Aufgaben besser "
"mitzuverfolgen."

#. (itstool) path: info/desc
#: C/gameplay.page:7
msgid "Introduction to <app>gbrainy</app>."
msgstr "Einführung in <app>gbrainy</app>."

#. (itstool) path: page/title
#: C/gameplay.page:20
msgid "Gameplay"
msgstr "Spielverlauf"

#. (itstool) path: page/p
#: C/gameplay.page:22
msgid ""
"<app>gbrainy</app> is a brain teaser game; the aim of the game is to have "
"fun and keep your brain trained."
msgstr ""
"<app>gbrainy</app> ist ein unterhaltsames Spiel mit dem Ziel, Spaß zu haben, "
"das Gehirn zu trainieren und fit zu halten."

#. (itstool) path: page/p
#: C/gameplay.page:26
msgid ""
"It features different game types like logic puzzles, mental calculation "
"games, memory trainers and verbal analogies, designed to test different "
"cognitive skills."
msgstr ""
"Es beinhaltet unterschiedliche Spielarten wie Logikrätsel, "
"Kopfrechenaufgaben, Gedächtnisübungen und verbale Analogien, die dafür "
"gedacht sind, verschiedene kognitive Fähigkeiten zu prüfen."

#. (itstool) path: page/p
#: C/gameplay.page:30
msgid ""
"You can choose different difficulty levels making <app>gbrainy</app> "
"enjoyable for kids, adults or senior citizens. It also features a game "
"history, personal records, tips and fullscreen mode support. <app>gbrainy</"
"app> can also be <link href=\"https://wiki.gnome.org/Apps/gbrainy/Extending"
"\">extended</link> easily with new games developed by third parties."
msgstr ""
"Sie können verschiedene Schwierigkeitsgrade wählen, um <app>gbrainy</app> "
"für Kinder, Erwachsene oder Senioren anzupassen. Es beinhaltet auch eine "
"Spielchronik, persönliche Rekorde, Hinweise und Unterstützung für "
"Vollbilddarstellung. <app>gbrainy</app> kann auch einfach mit neuen Spielen "
"von Drittanbietern <link href=\"https://wiki.gnome.org/Apps/gbrainy/Extending"
"\">erweitert</link> werden."

#. (itstool) path: page/p
#: C/gameplay.page:37
msgid ""
"<app>gbrainy</app> relies heavily on the work of previous puzzle masters, "
"ranging from classic puzzles from ancient times to more recent works like "
"<link href=\"https://en.wikipedia.org/wiki/Terry_Stickels\">Terry Stickels'</"
"link> puzzles or the classic <link href=\"https://en.wikipedia.org/wiki/Dr."
"_Brain\">Dr. Brain</link> game."
msgstr ""
"<app>gbrainy</app> beruht auf den Arbeiten ehemaliger Rätselmeister mit "
"Aufgaben aus früheren Zeiten bis hin zu jüngeren Arbeiten wie den Rätseln "
"von <link href=\"https://en.wikipedia.org/wiki/Terry_Stickels\">Terry "
"Stickels</link> oder dem klassischen Spiel <link href=\"https://en.wikipedia."
"org/wiki/Dr._Brain\">Dr. Brain</link>."

#. (itstool) path: note/p
#: C/gameplay.page:45
msgid ""
"There have been recent discussions in the scientific community regarding "
"whether brain training software improves cognitive performance. Most of the "
"studies show that there is little or no improvement, but that doesn't mean "
"you can't have a good time playing games like <app>gbrainy</app>!"
msgstr ""
"Erst kürzlich fanden wissenschaftliche Diskussionen statt, ob Gehirn-"
"Übungssoftware zu einer Verbesserung der kognitiven Leistung führen kann. "
"Die meisten dieser Studien zeigen, dass es zu keiner oder nur einer geringen "
"Verbesserung kommt. Das bedeutet aber nicht, dass man keinen Spaß mit "
"Spielen wie <app>gbrainy</app> haben kann!"

#. (itstool) path: info/title
#: C/index.page:8 C/index.page:12
msgctxt "link"
msgid "gbrainy"
msgstr "gbrainy"

#. (itstool) path: info/title
#: C/index.page:9
msgctxt "text"
msgid "gbrainy"
msgstr "gbrainy"

#. (itstool) path: info/desc
#: C/index.page:13
msgid "The <app>gbrainy</app> help."
msgstr "Die <app>gbrainy</app> Hilfe."

#. (itstool) path: page/title
#: C/index.page:23
msgid "<_:media-1/> gbrainy"
msgstr "<_:media-1/> gbrainy"

#. (itstool) path: section/title
#: C/index.page:29
msgid "Basic Gameplay &amp; Usage"
msgstr "Grundlagen des Spiels"

#. (itstool) path: section/title
#: C/index.page:33
msgid "Multiplayer Game"
msgstr "Mehrspielermodus"

#. (itstool) path: section/title
#: C/index.page:37
msgid "Tips &amp; Tricks"
msgstr "Tipps und Tricks"

#. (itstool) path: info/title
#: C/newgame.page:7
msgctxt "sort"
msgid "1"
msgstr "1"

#. (itstool) path: info/desc
#: C/newgame.page:10
msgid "Start and play a game."
msgstr "Ein Spiel starten und spielen."

#. (itstool) path: page/title
#: C/newgame.page:22
msgid "Game session"
msgstr "Spielsitzung"

#. (itstool) path: section/title
#: C/newgame.page:25
msgid "Start a new session"
msgstr "Eine neue Spielsitzung starten"

#. (itstool) path: section/p
#: C/newgame.page:26
msgid "To start a new game, do one of the following:"
msgstr "Um ein neues Spiel zu starten, führen Sie folgende Schritte durch:"

#. (itstool) path: item/p
#: C/newgame.page:31
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New "
"Game</gui></guiseq>, and select the type of game to play."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Spiel</gui><gui style=\"menuitem"
"\">Neues Spiel</gui></guiseq> und danach den Typ des gewünschten Spiels."

#. (itstool) path: item/p
#: C/newgame.page:37
msgid "Click on one of the buttons in the toolbar."
msgstr "Klicken Sie auf einen der Knöpfe in der Werkzeugleiste."

#. (itstool) path: section/p
#: C/newgame.page:42
msgid "The toolbar buttons have the following meanings:"
msgstr "Die Knöpfe der Werkzeugleiste haben folgende Funktionen:"

#. (itstool) path: item/title
#: C/newgame.page:50
msgid "All"
msgstr "Alle"

#. (itstool) path: item/p
#: C/newgame.page:51
msgid "Starts a new game playing all the available game types."
msgstr "Startet ein neues Spiel mit allen verfügbaren Spielarten."

#. (itstool) path: item/title
#: C/newgame.page:69
msgid "Logic"
msgstr "Logik"

#. (itstool) path: item/p
#: C/newgame.page:70
msgid "Start a new game playing only logic games."
msgstr "Startet ein neues Spiel, das nur Logikrätsel beinhaltet."

#. (itstool) path: item/title
#: C/newgame.page:88
msgid "Calculation"
msgstr "Rechnen"

#. (itstool) path: item/p
#: C/newgame.page:89
msgid "Starts a new game playing only calculation games."
msgstr ""
"Startet ein neues Spiel, in dem nur Übungen zum Rechnen enthalten sind."

#. (itstool) path: item/title
#: C/newgame.page:107
msgid "Memory"
msgstr "Gedächtnis"

#. (itstool) path: item/p
#: C/newgame.page:108
msgid "Starts a new game playing only memory games."
msgstr "Startet ein neues Spiel, das nur aus Gedächtnisübungen besteht."

#. (itstool) path: item/title
#: C/newgame.page:126
msgid "Verbal"
msgstr "Verbal"

#. (itstool) path: item/p
#: C/newgame.page:127
msgid "Starts a new game playing only verbal analogy games."
msgstr "Startet ein neues Spiel, das nur verbale Analogien enthält."

#. (itstool) path: note/p
#: C/newgame.page:143
msgid ""
"These descriptions also apply to the game types that you can select from the "
"<gui style=\"menu\">Game</gui> menu."
msgstr ""
"Diese Beschreibungen beziehen sich auch auf die Spielarten, die man im <gui "
"style=\"menu\">Spiel</gui>-Menü wählen kann."

#. (itstool) path: section/title
#: C/newgame.page:150
msgid "Play a session"
msgstr "Spielsitzungen"

#. (itstool) path: note/p
#: C/newgame.page:152
msgid "When you play a game, always read the instructions carefully!"
msgstr ""
"Lesen Sie sich die Anleitung immer genau durch, wenn Sie ein Spiel spielen!"

#. (itstool) path: section/p
#: C/newgame.page:156
msgid ""
"The game session begins by showing you the problem and then asking for the "
"answer. At the bottom of the window is the main set of controls that you can "
"use to interact with the game."
msgstr ""
"Die Spielsitzung beginnt mit der Anzeige der Problemstellung und der Frage "
"nach der Antwort. Am unteren Rand des Fensters finden Sie die "
"Hauptkontrollelemente zur Steuerung des Spiels."

#. (itstool) path: section/p
#: C/newgame.page:160
msgid ""
"Once you know the answer to the problem, type it in the <gui style=\"input"
"\">Answer</gui> text entry and press <gui style=\"button\">OK</gui>."
msgstr ""
"Sobald Sie die Antwort der Aufgabe wissen, schreiben Sie diese in das <gui "
"style=\"input\">Antwort</gui>-Eingabefeld und drücken Sie <gui style=\"button"
"\">OK</gui>."

#. (itstool) path: section/p
#: C/newgame.page:164
msgid "To proceed to the next game, click <gui style=\"button\">Next</gui>."
msgstr ""
"Um zum nächsten Spiel zu gelangen, klicken Sie auf <gui style=\"button"
"\">Weiter</gui>."

#. (itstool) path: note/p
#: C/newgame.page:168
msgid ""
"If you click <gui style=\"button\">Next</gui> before completing the game, or "
"without providing an answer, that game will be counted in your results."
msgstr ""
"Wenn Sie auf <gui style=\"button\">Weiter</gui> klicken, bevor die Aufgabe "
"gelöst worden ist oder ohne Angabe einer Antwort, so wird das Spiel in Ihren "
"Ergebnissen gezählt."

#. (itstool) path: info/desc
#: C/times.page:7
msgid "Change the challenge presentation time in memory games."
msgstr "Ändern der für Gedächtnisaufgaben zur Verfügung stehenden Zeit."

# Kürzer übersetzt. Darunter steht: Ändern der für Gedächtnisaufgaben zur Verfügung stehenden Zeit
#. (itstool) path: page/title
#: C/times.page:19
msgid "Change the challenge duration"
msgstr "Zeit für Aufgaben"

#. (itstool) path: page/p
#: C/times.page:20
msgid ""
"To change the number of seconds you are given to memorize the challenge when "
"playing memory games, proceed as follows:"
msgstr ""
"Um die Anzahl an Sekunden zu verändern, die zum Merken einer "
"Gedächtnisaufgabe zur Verfügung stehen, gehen Sie wie folgt vor:"

#. (itstool) path: item/p
#: C/times.page:31
msgid ""
"In the <gui>Memory Games</gui> section, use the spin box to increase or "
"decrease the number of seconds that the challenge will be visible for."
msgstr ""
"Verwenden Sie das Einstellfeld im Abschnitt <gui>Gedächtnisspiele</gui>, um "
"die Dauer in Sekunden, solange die Aufgabe angezeigt wird, zu erhöhen oder "
"zu erniedrigen."

#. (itstool) path: item/p
#: C/times.page:35
msgid "The default value is 6 seconds."
msgstr "Der Standardwert ist 6 Sekunden."

#. (itstool) path: info/desc
#: C/export.page:7
msgid "Export the games for playing off-line."
msgstr "Spiele exportieren, um offline zu spielen."

#. (itstool) path: page/title
#: C/export.page:20
msgid "Export the games"
msgstr "Spiele exportieren"

#. (itstool) path: page/p
#: C/export.page:21
msgid ""
"It is possible to export and then print the games provided by the app for "
"playing while not at the computer."
msgstr ""
"Es ist möglich, die Spiele von <app>gbrainy</app> zu exportieren und zu "
"drucken, um sie ohne Rechner spielen zu können."

#. (itstool) path: page/p
#: C/export.page:25
msgid ""
"The games will be saved in a <file>PDF</file> file in the selected folder."
msgstr ""
"Die Spiele werden in einer <file>PDF</file>-Datei im ausgewählten Ordner "
"gespeichert."

# Siehe Standardübersetzungen
#. (itstool) path: page/p
#: C/export.page:28
msgid "To export the games:"
msgstr "So exportieren Sie Spiele:"

#. (itstool) path: item/p
#: C/export.page:33
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">Export "
"Games to PDF for Off-Line Playing</gui></guiseq>."
msgstr ""
"Wählen Sie <guiseq><gui style=\"menu\">Spiel</gui><gui style=\"menuitem"
"\">Spiele als PDF-Datei exportieren …</gui></guiseq>."

#. (itstool) path: item/p
#: C/export.page:38
msgid ""
"Select the type of games, the difficulty level, and how many games to "
"export. When done click <gui style=\"button\">Save</gui>."
msgstr ""
"Wählen Sie den Spieltyp, Schwierigkeitsgrad und die Anzahl der zu "
"exportierenden Spiele. Klicken Sie anschließend auf <gui style=\"button"
"\">Speichern</gui>."
