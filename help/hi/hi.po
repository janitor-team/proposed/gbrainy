msgid ""
msgstr ""
"Project-Id-Version: gbrainy documentation\n"
"POT-Creation-Date: 2010-05-10 21:20+0000\n"
"PO-Revision-Date: 2010-05-10 21:20+0000\n"
"Last-Translator: DILIP THIRUPATHISAMY <u4764938@anu.edu.au>\n"
"Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: C/legal.xml:3(p)
msgid "This work is licensed under a <link href=\"http://creativecommons.org/licenses/by-sa/3.0/\">Creative Commons Attribution-Share Alike 3.0 Unported License</link>."
msgstr ""

#: C/legal.xml:6(p)
msgid "As a special exception, the copyright holders give you permission to copy, modify, and distribute the example code contained in this document under the terms of your choosing, without restriction."
msgstr ""

#: C/times.page:7(desc)
msgid "Change the challenge presentation time in memory games."
msgstr "स्मृति खेल में चुनौती प्रस्तुति का  समय बदलें"

#: C/times.page:11(name) C/newgame.page:13(name) C/index.page:14(name) C/gameplay.page:12(name) C/score.page:11(name) C/multiplayer.page:11(name) C/history.page:9(name) C/difficulty.page:12(name) C/tips.page:12(name) C/pause.page:12(name) C/customgame.page:12(name) C/index.page:14(name)
msgid "Milo Casagrande"
msgstr ""

#: C/times.page:12(email) C/newgame.page:14(email) C/index.page:15(email) C/gameplay.page:13(email) C/score.page:12(email) C/multiplayer.page:12(email) C/history.page:10(email) C/difficulty.page:13(email) C/tips.page:13(email) C/pause.page:13(email) C/customgame.page:13(email) C/index.page:15(email)
msgid "milo@ubuntu.com"
msgstr ""

#: C/times.page:15(p) C/newgame.page:17(p) C/index.page:18(p) C/gameplay.page:16(p) C/score.page:15(p) C/multiplayer.page:15(p) C/history.page:13(p) C/difficulty.page:16(p) C/tips.page:16(p) C/pause.page:16(p) C/gametypes.page:12(p) C/customgame.page:16(p) C/index.page:18(p)
msgid "Creative Commons Share Alike 3.0"
msgstr ""

#: C/times.page:19(title)
msgid "Change the challenge duration"
msgstr "चुनौती अवधि को बदलें "

#: C/times.page:20(p)
msgid "To change the number of seconds you are given to memorize the challenge when playing memory games, proceed as follows:"
msgstr "स्मृति खेल की चुनौती के समय को बदलने के लिए निम्नानुसार है:"

#: C/times.page:26(p) C/difficulty.page:26(p)
msgid "Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem\">Preferences</gui></guiseq>."
msgstr ""

#: C/times.page:31(p)
msgid "In the <gui>Memory Games</gui> section, use the spin box to increase or decrease the number of seconds that the challenge will be visible for."
msgstr ""

#: C/times.page:35(p)
msgid "The default value is 4 seconds."
msgstr "मूलभूत मान 4 सेकंड हैं"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/newgame.page:66(None)
msgid "@@image: 'figures/all-games.png'; md5=b9b2464dfdb280d8de79b921e03a4705"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/newgame.page:85(None) C/gametypes.page:36(None)
msgid "@@image: 'figures/logic-games.png'; md5=2679be12956397b6337da869bdbee82d"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/newgame.page:104(None) C/gametypes.page:57(None)
msgid "@@image: 'figures/math-games.png'; md5=208d189bcd6246b40e3dff184a5d5698"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/newgame.page:123(None) C/gametypes.page:78(None)
msgid "@@image: 'figures/memory-games.png'; md5=2747c731e0a757a98a91c8e141748c75"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/newgame.page:142(None) C/gametypes.page:98(None)
msgid "@@image: 'figures/verbal-games.png'; md5=6559e8d03907794b9983c5501df1aed2"
msgstr ""

#: C/newgame.page:6(title)
msgid "1"
msgstr "१ "

#: C/newgame.page:9(desc)
msgid "Start and play a game."
msgstr "खेल आरंभ करें "

#: C/newgame.page:21(title)
msgid "Game session"
msgstr "खेल सत्र"

#: C/newgame.page:24(title)
msgid "Start a new session"
msgstr "एक नया सत्र शुरू करें "

#: C/newgame.page:25(p)
msgid "To start a new game, do one of the following:"
msgstr "एक नया खेल शुरू करने के लिए, निम्न में से एक कार्य करें:"

#: C/newgame.page:30(p)
msgid "Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New Game</gui></guiseq>, and select the type of game to play."
msgstr ""

#: C/newgame.page:36(p)
msgid "Click on one of the buttons in the toolbar."
msgstr "उपकरण पट्टी में एक बटन को दबाएँ"

#: C/newgame.page:41(p)
msgid "The toolbar buttons have the following meanings:"
msgstr "उपकरण पट्टी में जो बटन होती हैं  उनका अर्थ निम्नलिखित है:"

#: C/newgame.page:45(cite) C/newgame.page:176(cite) C/score.page:100(cite)
msgid "Phil Bull"
msgstr "फिल सिपाही"

#: C/newgame.page:46(p)
msgid "The descriptions in the list below just echo the name of each button (X: Start a new game playing X games). Can we do anything more useful with them (e.g. link to descriptions of each game type, or give examples of the sorts of games in each category?)"
msgstr ""

#: C/newgame.page:58(title)
msgid "All"
msgstr "सभी"

#: C/newgame.page:59(p)
msgid "Starts a new game playing all the available game types."
msgstr "नया खेल शुरू होता है  खेल के सभी प्रकार के उपलब्ध खेल खेल सकते हैं "

#: C/newgame.page:67(p)
msgid "All games button"
msgstr "सभी खेल बटन"

#: C/newgame.page:77(title)
msgid "Logic"
msgstr "तर्क"

#: C/newgame.page:78(p)
msgid "Start a new game playing only logic games."
msgstr "केवल तर्क खेलों का नया  खेल शुरू करें"

#: C/newgame.page:86(p)
msgid "Logic games button"
msgstr "तर्क खेल बटन"

#: C/newgame.page:96(title)
msgid "Calculation"
msgstr "गणना"

#: C/newgame.page:97(p)
msgid "Starts a new game playing only calculation games."
msgstr "केवल गणना खेलों का नया खेल शुरू होता  है"

#: C/newgame.page:105(p)
msgid "Calculation games button"
msgstr "गणना खेल बटन"

#: C/newgame.page:115(title)
msgid "Memory"
msgstr "स्मृति"

#: C/newgame.page:116(p)
msgid "Starts a new game playing only memory games."
msgstr "केवल स्मृति खेलों का नया खेल शुरू होता  है"

#: C/newgame.page:124(p)
msgid "Memory games button"
msgstr "स्मृति खेल बटन"

#: C/newgame.page:134(title)
msgid "Verbal"
msgstr "मौखिक"

#: C/newgame.page:135(p)
msgid "Starts a new game playing only verbal analogy games."
msgstr "केवल मौखिक खेलों का नया खेल शुरू होता  है"

#: C/newgame.page:143(p)
msgid "Verbal games button"
msgstr "मौखिक खेल बटन"

#: C/newgame.page:151(p)
msgid "These descriptions also apply to the game types that you can select from the <gui style=\"menu\">Game</gui> menu."
msgstr ""

#: C/newgame.page:158(title)
msgid "Play a session"
msgstr "एक सत्र खेलें"

#: C/newgame.page:160(p)
msgid "When you play a game, always read the instructions carefully!"
msgstr "जब आप एक खेल खेलते हैं, हमेशा निर्देशों का सावधानी से पढ़ें !"

#: C/newgame.page:164(p)
msgid "The game session begins by showing you the problem and then asking for the answer. At the bottom of the window is the main set of controls that you can use to interact with the game."
msgstr ""

#: C/newgame.page:168(p)
msgid "Once you know the answer to the problem, type it in the <gui style=\"input\">Answer</gui> text entry and press <gui style=\"button\">OK</gui>."
msgstr ""

#: C/newgame.page:172(p)
msgid "To proceed to the next game, click <gui style=\"button\">Next</gui>."
msgstr ""

#: C/newgame.page:177(p)
msgid "I don't understand what this note means. Does it mean that the game will count negatively?"
msgstr ""

#: C/newgame.page:182(p)
msgid "If you click <gui style=\"button\">Next</gui> before completing the game, or without providing an answer, that game will be counted in your results."
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/index.page:23(None) C/index.page:23(None)
msgid "@@image: 'figures/gbrainy.png'; md5=d3b523ab1c8d00d00560d3f036d681bb"
msgstr ""

#: C/index.page:7(title) C/index.page:8(title) C/index.page:11(title) C/index.page:7(title) C/index.page:8(title) C/index.page:11(title)
msgid "gbrainy"
msgstr "जी बरैनी"

#: C/index.page:12(desc) C/index.page:12(desc)
msgid "The <app>gbrainy</app> help."
msgstr ""

#: C/index.page:22(title) C/index.page:22(title)
msgid "<media type=\"image\" mime=\"image/png\" src=\"figures/gbrainy.png\">gbrainy logo</media> gbrainy"
msgstr ""

#: C/index.page:28(title) C/index.page:28(title)
msgid "Basic Gameplay &amp; Usage"
msgstr "बुनियादी खेल ; प्रयोग"

#: C/index.page:32(title) C/index.page:32(title)
msgid "Multiplayer Game"
msgstr "बहु खिलाड़ी खेल"

#: C/index.page:36(title) C/index.page:36(title)
msgid "Tips &amp; Tricks"
msgstr ""

#: C/gameplay.page:7(desc)
msgid "Introduction to <app>gbrainy</app>."
msgstr "<app>जी बरैनी </app>का परिचय"

#: C/gameplay.page:20(title)
msgid "Gameplay"
msgstr "खेल खेलना "

#: C/gameplay.page:22(p)
msgid "<app>gbrainy</app> is a brain teaser game; the aim of the game is to have fun and keep your brain trained."
msgstr "<app>जी बरैनी</app> एक मस्तिष्क चिढ़ाने का खेल है; खेल का उद्देश्य है कि खेल मजेदार हो और अपने मस्तिष्क को प्रशिक्षित रखने के लिए है"

#: C/gameplay.page:26(p)
msgid "It features different game types like logic puzzles, mental calculation games, memory trainers and verbal analogies, designed to test different cognitive skills."
msgstr "यह तर्क पहेली खेल की तरह विभिन्न प्रकार सुविधाओं, मानसिक गणना खेल,स्मृति प्रशिक्षकों और मौखिक समानता,अलग संज्ञानात्मक कौशल का परीक्षण किया गया है"

#: C/gameplay.page:30(p)
msgid "You can choose different difficulty levels making <app>gbrainy</app> enjoyable for kids, adults or senior citizens. It also features a game history, personal records, tips and fullscreen mode support. <app>gbrainy</app> can also be <link href=\"http://live.gnome.org/gbrainy/Extending\">extended</link> easily with new games developed by third parties."
msgstr ""

#: C/gameplay.page:37(p)
msgid "<app>gbrainy</app> relies heavily on the work of previous puzzle masters, ranging from classic puzzles from ancient times to more recent works like <link href=\"http://en.wikipedia.org/wiki/Terry_Stickels\">Terry Stickels'</link> puzzles or the classic <link href=\"http://en.wikipedia.org/wiki/Dr._Brain\">Dr. Brain</link> game."
msgstr ""

#: C/gameplay.page:45(p)
msgid "There have been recent discussions in the scientific community regarding whether brain training software improves cognitive performance. Most of the studies show that there is little or no improvement, but that doesn't mean you can't have a good time playing games like <app>gbrainy</app>!"
msgstr ""

#: C/score.page:8(desc)
msgid "How the player scores are calculated."
msgstr "खिलाड़ी के अंक को कैसे गणना करते हैं "

#: C/score.page:19(title)
msgid "Game score and timings"
msgstr "खेल का अंक और समय"

#: C/score.page:20(p)
msgid "If you answer a puzzle incorrectly, you will not get any score for it."
msgstr "यदि आप एक पहेली गलत जवाब,आपको इसके लिए कोई अंक नहीं मिलेगा"

#: C/score.page:23(p)
msgid "If you answer a puzzle correctly, you will get a score which depends on the time taken to resolve the problem and whether you used the tip during the game."
msgstr ""

#: C/score.page:27(p)
msgid "The following table summarizes the different game durations (in seconds) based on the difficulty level."
msgstr ""

#: C/score.page:34(p)
msgid "Easy"
msgstr "आसान"

#: C/score.page:35(p)
msgid "Medium"
msgstr "मध्यम"

#: C/score.page:36(p)
msgid "Master"
msgstr "माहिर"

#: C/score.page:39(p) C/gametypes.page:26(title)
msgid "Logic puzzles"
msgstr "तर्क पहेलियाँ"

#: C/score.page:40(p)
msgid "156"
msgstr "१५६"

#: C/score.page:41(p)
msgid "120"
msgstr "१२०"

#: C/score.page:42(p)
msgid "110"
msgstr "११०"

#: C/score.page:45(p)
msgid "Mental calculation"
msgstr "मानसिक गणना"

#: C/score.page:46(p)
msgid "78"
msgstr "७८"

#: C/score.page:47(p)
msgid "60"
msgstr "६०"

#: C/score.page:48(p)
msgid "55"
msgstr "५५"

#: C/score.page:51(p) C/gametypes.page:68(title)
msgid "Memory trainers"
msgstr "स्मृति प्रशिक्षकों"

#: C/score.page:52(p) C/score.page:58(p)
msgid "39"
msgstr "३९"

#: C/score.page:53(p) C/score.page:59(p)
msgid "30"
msgstr "३०"

#: C/score.page:54(p) C/score.page:60(p)
msgid "27"
msgstr "२७"

#: C/score.page:57(p) C/gametypes.page:89(title)
msgid "Verbal analogies"
msgstr "मौखिक समानता"

#: C/score.page:63(p)
msgid "With the expected time for the chosen difficulty level and the time you take to complete the game, the following logic is applied:"
msgstr ""

#: C/score.page:69(p)
msgid "If you take less than the time expected to complete the game, you score 100%."
msgstr ""

#: C/score.page:74(p)
msgid "If you take more than the time expected to complete the game, you score 80%."
msgstr ""

#: C/score.page:79(p)
msgid "If you take more than 2x the time expected to complete the game, you score 70%."
msgstr ""

#: C/score.page:85(p)
msgid "If you take more than 3x the time expected to complete the game, you score 60%."
msgstr ""

#: C/score.page:91(p)
msgid "If you use a tip, you score only the 80% of the original score."
msgstr ""

#: C/score.page:98(title)
msgid "Computing the totals"
msgstr "कंप्यूटिंग योग"

#: C/score.page:101(p)
msgid "In this section, does \"logarithm of 20\" mean score = log_20(sum(results))?"
msgstr ""

#: C/score.page:105(p)
msgid "<app>gbrainy</app> keeps track of the different games types played. To compute the final score of every set of game types it sums all the results of the same game types played and then applies a factor based on: the logarithm of 10 for the easy level; on the logarithm of 20 for the medium level; and on the logarithm of 30 for the master level."
msgstr ""

#: C/score.page:112(p)
msgid "This means that when playing at medium difficult level, to get a score of 100% you need to score 100% on at least 20 games of every game type played."
msgstr ""

#: C/score.page:116(p)
msgid "This may sound challenging, but it allows players to compare game scores from different sessions (in the player's game history) and allows better tracking of the progression of the player through all of the games played."
msgstr ""

#: C/multiplayer.page:7(desc)
msgid "How to play with other people."
msgstr "अन्य लोगों के साथ कैसे खेलें "

#: C/multiplayer.page:19(title)
msgid "Play with other people"
msgstr "अन्य लोगों के साथ खेलो"

#: C/multiplayer.page:21(p)
msgid "It is not possible to play against other people over the Internet or a local network with <app>gbrainy</app>."
msgstr "इंटरनेट या एक स्थानीय नेटवर्क में <app>जी बरैनी </app> खेला नहीं जा सकता"

#: C/history.page:6(desc)
msgid "Access your personal game history."
msgstr "अपने व्यक्तिगत खेल इतिहास का उपयोग करें "

#: C/history.page:17(title)
msgid "Personal game history"
msgstr "व्यक्तिगत खेल इतिहास"

#: C/history.page:19(p)
msgid "The <gui>Player's Game History</gui> dialog shows your performance in every game type during the last few game sessions."
msgstr ""

#: C/history.page:25(title)
msgid "View the games history"
msgstr ""

#: C/history.page:26(p)
msgid "To access your personal game history, choose <guiseq><gui style=\"menu\">View</gui><gui style=\"menuitem\">Player's Game History</gui></guiseq>."
msgstr ""

#: C/history.page:33(title)
msgid "Select which results to show"
msgstr "परिणाम देखने का चयन करें"

#: C/history.page:34(p)
msgid "To select the results that will be shown in the graphic, use the different check boxes located under the graph."
msgstr ""

#: C/history.page:41(title)
msgid "Change the number of saved games"
msgstr "पहले खेले गए खेलों की संख्या को बदलें "

#: C/history.page:42(p)
msgid "<app>gbrainy</app> saves the player's scores so it can track how they evolve."
msgstr "<app>जी बरैनी</app>खिलाड़ी के अंक का संग्रह करते यह कह सकता है वे कैसे विकसित हैं"

#: C/history.page:45(p)
msgid "In order to change the number of sessions recorded or how many games will be stored in the history, proceed as follows:"
msgstr ""

#: C/history.page:51(p)
msgid "Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem\"> Preferences</gui></guiseq>."
msgstr ""

#: C/history.page:57(p)
msgid "In the <gui>Player's Game History</gui> section:"
msgstr ""

#: C/history.page:62(p)
msgid "Use the first spin box to change how many game sessions need to be recorded before you can start to see the results in the history graphic."
msgstr ""

#: C/history.page:68(p)
msgid "Use the second spin box to change how many game sessions will be shown in the history graphic."
msgstr ""

#: C/difficulty.page:6(title)
msgid "2"
msgstr "२"

#: C/difficulty.page:8(desc)
msgid "Change the difficulty level of the games."
msgstr "खेल की कठिनाई का स्तर बदलें"

#: C/difficulty.page:20(title)
msgid "Difficulty levels"
msgstr "कठिनाई का स्तर"

#: C/difficulty.page:21(p)
msgid "To change the difficulty level of the game, proceed as follows:"
msgstr "कठिनाई की स्तर बदलने का रूप निम्नानुसार है"

#: C/difficulty.page:31(p)
msgid "In the <gui>Difficulty Level</gui> section, select the desired level."
msgstr ""

#: C/difficulty.page:34(p)
msgid "You can choose from three different levels: <gui style=\"radiobutton\">Easy</gui>, <gui style=\"radiobutton\">Medium</gui>, and <gui style=\"radiobutton\">Master</gui>. The default level is <gui style=\"radiobutton\">Medium</gui>."
msgstr ""

#: C/tips.page:8(desc)
msgid "Use the tips to solve a puzzle."
msgstr "पहेली को हल करने के लिए युक्तियों का उपयोग करें "

#: C/tips.page:20(title)
msgid "Tips"
msgstr "युक्तियाँ "

#: C/tips.page:21(p)
msgid "With some of the games you can play, it is possible to get simple tips that can help you to solve the problem."
msgstr ""

#: C/tips.page:25(p)
msgid "When playing a game, click on the <gui style=\"button\">Tip</gui> button at the bottom of the window (next to the <gui style=\"input\">Answer</gui> text entry)."
msgstr ""

#: C/tips.page:29(p)
msgid "This feature is not available for some of the games."
msgstr "कुछ खेल में यह सुविधा उपलब्ध नहीं है"

#: C/pause.page:6(title)
msgid "4"
msgstr "४ "

#: C/pause.page:9(desc)
msgid "How to pause or stop a game."
msgstr "खेल को कैसे ठहरा या रोका जा सकता है "

#: C/pause.page:20(title)
msgid "Pause/Stop a game"
msgstr "खेल की ठहराव / रोकना"

#: C/pause.page:23(title)
msgid "Pause and resume a game"
msgstr "विरामित और फिर खेल का अरम्ब"

#: C/pause.page:24(p)
msgid "To pause a game so that you can resume it at the same point at a later time, perform one of the following:"
msgstr ""

#: C/pause.page:30(p) C/pause.page:45(p)
msgid "Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">Pause Game</gui></guiseq>."
msgstr ""

#: C/pause.page:35(p)
msgid "Click on the <gui style=\"button\">Pause</gui> button in the toolbar."
msgstr ""

#: C/pause.page:40(p)
msgid "In order to resume the game after you paused it, perform one of the following:"
msgstr ""

#: C/pause.page:50(p)
msgid "Click on the <gui style=\"button\">Resume</gui> button in the toolbar."
msgstr ""

#: C/pause.page:58(title)
msgid "Stop a game"
msgstr "खेल को रोकना  "

#: C/pause.page:59(p)
msgid "To stop a game, in order to end it, perform one of the following:"
msgstr "खेल रोकने के लिए, निम्न में से एक करें :"

#: C/pause.page:64(p)
msgid "Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">End Game</gui></guiseq>."
msgstr ""

#: C/pause.page:69(p)
msgid "Click on the <gui style=\"button\">Finish</gui> button in the toolbar."
msgstr ""

#: C/license.page:8(desc)
msgid "Legal information."
msgstr "कानूनी जानकारी"

#: C/license.page:11(title)
msgid "License"
msgstr "अनुज्ञा पत्र"

#: C/license.page:12(p)
msgid "This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 Unported license."
msgstr ""

#: C/license.page:18(p)
msgid "You are free:"
msgstr "आप स्वतंत्र हैं:"

#: C/license.page:23(em)
msgid "To share"
msgstr "To share"

#: C/license.page:24(p)
msgid "To copy, distribute and transmit the work."
msgstr "नकल करने के लिए, वितरित और काम संचारित"

#: C/license.page:27(em)
msgid "To remix"
msgstr "रीमिक्स करने के लिए"

#: C/license.page:28(p)
msgid "To adapt the work."
msgstr "काम करने के लिए अनुकूलित"

#: C/license.page:31(p)
msgid "Under the following conditions:"
msgstr "निम्नलिखित शर्तों के तहत"

#: C/license.page:36(em)
msgid "Attribution"
msgstr "आरोपण"

#: C/license.page:37(p)
msgid "You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work)."
msgstr ""

#: C/license.page:44(em)
msgid "Share Alike"
msgstr ""

#: C/license.page:45(p)
msgid "If you alter, transform, or build upon this work, you may distribute the resulting work only under the same, similar or a compatible license."
msgstr ""

#: C/license.page:51(p)
msgid "For the full text of the license, see the <link href=\"http://creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</link>, or read the full <link href=\"http://creativecommons.org/licenses/by-sa/3.0/\">Commons Deed</link>."
msgstr ""

#: C/gametypes.page:5(title)
msgid "3"
msgstr "३"

#: C/gametypes.page:6(desc)
msgid "What types of game you can play."
msgstr "कौन से प्रकार के खेल आप खेल सकते हैं "

#: C/gametypes.page:15(title)
msgid "Game types"
msgstr "खेल के प्रकार"

#: C/gametypes.page:17(p)
msgid "<app>gbrainy</app> provides the following types of games:"
msgstr "<app>जी बरैनी </app>खेल के निम्नलिखित प्रकार प्रदान करता है:"

#: C/gametypes.page:27(p)
msgid "Games designed to challenge your reasoning and thinking skills. These games are based on sequences of elements, visual and spatial reasoning or relationships between elements."
msgstr ""

#: C/gametypes.page:37(p)
msgid "Logic games logo"
msgstr "तर्क खेल की लोगो"

#: C/gametypes.page:47(title)
msgid "Mental calculations"
msgstr "मानसिक गणना"

#: C/gametypes.page:48(p)
msgid "Games based on arithmetical operations designed to improve your mental calculation skills. Games that require the player to use multiplication, division, addition and subtraction combined in different ways."
msgstr ""

#: C/gametypes.page:58(p)
msgid "Calculation games logo"
msgstr "गणना खेल की लोगो"

#: C/gametypes.page:69(p)
msgid "Games designed to challenge your short-term memory. These games show collections of objects and ask the player to recall them, sometimes establishing relationships between figures, words, numbers or colors."
msgstr ""

#: C/gametypes.page:79(p)
msgid "Memory games logo"
msgstr "स्मृति खेल की लोगो"

#: C/gametypes.page:90(p)
msgid "Games that challenge your verbal aptitude. These games ask the player to identify cause and effect, use synonyms or antonyms, and use their vocabulary."
msgstr ""

#: C/gametypes.page:99(p)
msgid "Verbal analogies games logo"
msgstr "मौखिक समानता खेल की लोगो"

#: C/customgame.page:9(desc)
msgid "Start a custom games selection."
msgstr "रिवाज खेल का चयन अरम्ब करें"

#: C/customgame.page:20(title)
msgid "Custom games selection"
msgstr "रिवाज खेलों का चयन"

#: C/customgame.page:21(p)
msgid "It is possible to play a custom selection of all the games in <app>gbrainy</app>."
msgstr ""

#: C/customgame.page:24(p)
msgid "To do so, proceed as follows:"
msgstr "ऐसा करने के लिए, आगे बढ़ने के रूप निम्नानुसार है"

#: C/customgame.page:29(p)
msgid "Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New Game</gui><gui style=\"menuitem\">Custom Game Selection</gui></guiseq>."
msgstr ""

#: C/customgame.page:35(p)
msgid "In the <gui>Custom Game</gui> dialog, select the games that you would like to play by clicking on the check box next to the name of the game."
msgstr ""

#: C/customgame.page:41(p)
msgid "Once you are done, click on <gui style=\"button\">Start</gui> to start playing your custom games selection."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: C/index.page:0(None)
msgid "translator-credits"
msgstr "अनुवादक-ऋण"


