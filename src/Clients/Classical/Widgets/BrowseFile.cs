/*
 * Copyright (C) 2009 Jordi Mas i Hernàndez <jmas@softcatala.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Gtk;
using Mono.Unix;


namespace gbrainy.Clients.Classical.Widgets
{
	// Adds a text box + browse button into a given hbox parent configuring
	// the standard browsedirectory widget for the application
	public class BrowseFile
	{
		Entry filename;
		Button browse;
		Gtk.FileFilter[] filters;
                string default_dir;

		public virtual event EventHandler FileSelectedChanged;

		public BrowseFile (Box parent, string file, string default_dir)
		{
		        this.default_dir = default_dir;
			filename = new Entry ();
			browse = new Button (Catalog.GetString ("Browse..."));
			Filename = file;

			browse.Clicked += new EventHandler (OnBrowse);

			parent.Add (filename);
			parent.Add (browse);

			parent.ShowAll ();
		}

		public string Filename {
			get { return filename.Text; }
			set { 
				if (value == null)
					filename.Text = string.Empty;
				else
					filename.Text = value;
			}
		}

		public Gtk.FileFilter[] Filters {
			set { filters = value; }
		}

		void OnBrowse (object o, EventArgs args)
		{
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Open Location") , null,
				FileChooserAction.Save);

                        chooser_dialog.SetCurrentFolder (this.default_dir);
                        chooser_dialog.CurrentName = filename.Text;
			chooser_dialog.AddButton (Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Stock.Save, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = false;

			if (filters != null) {
				foreach (Gtk.FileFilter filter in filters)
					chooser_dialog.AddFilter (filter);
			}

			if (chooser_dialog.Run () == (int) ResponseType.Ok) {
				filename.Text = chooser_dialog.Filename;

				if (FileSelectedChanged != null)
					FileSelectedChanged (this, EventArgs.Empty);
			}

			chooser_dialog.Destroy ();
		}
	}
}
